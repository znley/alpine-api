#!/usr/bin/python3

import alpine

usernames = ['huajingyun01', 'znley']


iids = []
for username in usernames:
    iids.extend(alpine.get_merge_request_opened(username))

for iid in iids:
    print(iid)

    changes = alpine.get_merge_request_changes(iid)

    with open('opened/'+str(iid)+'.patch', mode='w', encoding='utf-8') as f:
        for change in changes:
            f.write(change)
    f.close()
