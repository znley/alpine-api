#!/usr/bin/python3

import requests
import json
import csv

# https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-requests
# GET /merge_requests?author_username=znley

# community/ezstream: update config guess to support loongarch64
def split_title(title):
    strs = title.split(':')
    return strs

def get_merge_requests(username):
    mrs = [] # [{title: 'community/openjdk17', state: merged, url: ''}]
    page = 1
    url = 'https://gitlab.alpinelinux.org/api/v4/projects/1/merge_requests'
    #headers = {"Authorization": token}
    headers = {}

    while True:
        params = {'author_username': username, 'per_page': 20, 'page': page}
        # https://docs.python-requests.org/en/latest/api/#requests.request
        response = requests.request('GET', url,  params=params, headers=headers)

        jdata = response.json()
        if len(jdata) == 0:
            break
        else:
            page=page+1

        #
        for mr in jdata:
            mr_id = mr["iid"]

            stitle = split_title(mr["title"])
            if len(stitle) != 2:
                # 格式不正确，不考虑
                print(mr["title"])
                continue

            diffs = get_merge_request_diffs(mr_id)

            item = {
                "iid": mr["iid"],
                "title": stitle[0],
                "state": mr["state"],
                "add": diffs["add"],
                "del": diffs["del"],
                "msg": stitle[1].strip().replace(" ", "-"),
                "web_url": mr["web_url"]
                }
            mrs.append(item)

            print(item)
            #mrs.append({
            #    "iid": mr["iid"],
            #    "title": stitle[0]),
            #    "state": mr["state"],
            #    "add": diffs["add"],
            #    "del": diffs["del"],
            #    "msg": stitle[1].replace(" ", "-"),
            #    "web_url": mr["web_url"],
            #    })
        
    return mrs
    
def get_merge_request_opened(username):
    mrs = []
    page = 1
    url = 'https://gitlab.alpinelinux.org/api/v4/projects/1/merge_requests'

    while True:
        params = {'author_username': username, 'per_page': 20, 'page': page, 'state': "opened"}
        # https://docs.python-requests.org/en/latest/api/#requests.request
        response = requests.request('GET', url,  params=params)

        # type of jdata is list
        jdata = response.json()
        if len(jdata) == 0:
            break
        else:
            page=page+1

        for mr in jdata:
            mrs.append(mr["iid"])

    return mrs

def get_merge_request_changes(iid):
    changes = []
    url = 'https://gitlab.alpinelinux.org/api/v4/projects/1/merge_requests/' + str(iid) + '/changes'

    params = {'unidiff': 'true'}
    headers = {}
    response = requests.request('GET', url, params = params, headers = headers)

    # type of jdata is dict
    jdata = response.json()

    # https://docs.gitlab.com/ee/api/merge_requests.html#get-single-merge-request-changes
    # type of changes is list
    jchanges = jdata["changes"]

    for jchange in jchanges:
        changes.append(jchange["diff"])

    return changes


def get_merge_request_diffs(iid):
    url = 'https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/' + str(iid) + '/diffs_metadata.json'
    response = requests.request('GET', url)

    jdata = response.json()

    return {
            "add": jdata["added_lines"],
            "del": jdata["removed_lines"]
            }

def csv_write_mrs(name, mrs):
    with open(name, 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        
        #   "title": simplify_title(mr["title"]),
        #   "iid": mr["iid"],
        #   "state": mr["state"],
        #   "web_url": mr["web_url"],
        #   "add": diffs["add"],
        #   "del": diffs["del"]
        for mr in mrs:
            spamwriter.writerow([
                mr["iid"], 
                mr["title"],
                mr["state"],
                mr["add"],
                mr["del"],
                mr["msg"],
                mr["web_url"]
                ])


#username = 'znley'
#token = 'glpat-PxD1RyApxPGyPwYuh8E4'
#stime = time.strftime('%Y%m%d-%H%M%S', time.localtime())
#
#mrs = get_merge_requests(username, token)
#
#csv_write_mrs(username+"-"+stime+'.csv', mrs)
#
#print(len(mrs))
#print(mrs)

#csv_write_mrs("test.csv", [{"iid": 1, "title": "aaa", "state": "closed", "add": 5, "del": 1, "web_url": "http://baidu.com"}])
#out = get_merge_request_diffs(65731)
#print(out)

