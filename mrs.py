#!/usr/bin/python3

import alpine
import time

username = 'huajingyun01'
#username = 'znley'

stime = time.strftime('%Y%m%d-%H%M%S', time.localtime())
mrs = alpine.get_merge_requests(username)
alpine.csv_write_mrs(username+"-"+stime+'.csv', mrs)
